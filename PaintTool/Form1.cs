﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintTool
{
    public partial class Form1 : Form
    {
        private Bitmap m_bmpCanvas;
        bool isMouseDown = false;
        Graphics gr;
        Point lastPoint = Point.Empty;


        public Form1()
        {
            InitializeComponent();

            m_bmpCanvas = new Bitmap("./canvas.bmp");
        }
                
        private void ToolBoxList_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = e.Location;
            isMouseDown = true;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            lastPoint = Point.Empty;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                if(lastPoint != null)
                {
                    if(Panel.Image == null)
                    {
                        Bitmap bmp = new Bitmap(Panel.Width, Panel.Height);
                        Panel.Image = bmp;
                    }

                    using (Graphics g = Graphics.FromImage(Panel.Image))
                    {
                        g.DrawLine(new Pen(CurrentColour.BackColor, float.Parse(LineWidth.Text)), lastPoint, e.Location);
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    }
                }
                Panel.Invalidate();
                lastPoint = e.Location;
            }            
        }

        private void CurrentColour_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();

            if (c.ShowDialog() == DialogResult.OK)
            {
                CurrentColour.BackColor = c.Color;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Panel.BackColor = Color.White;

            CurrentColour.BackColor = Color.Black;
            BackgroundColour.BackColor = Color.White;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void BackgroundColour_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();

            if (c.ShowDialog() == DialogResult.OK)
            {
                BackgroundColour.BackColor = c.Color;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Image Files(*.bmp)|*.bmp;|All files(*.*)|*.*";
            saveFile.Title = "Save current Image";

            saveFile.ShowDialog();

            if(saveFile.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFile.OpenFile();
                m_bmpCanvas.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Panel.SizeMode = PictureBoxSizeMode.AutoSize;
            Panel.Location = new Point(136, 34);

            Bitmap bm = new Bitmap(280, 110);
            using (gr = Graphics.FromImage(bm))
            {
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                Rectangle rect = new Rectangle(10, 10, 260, 90);
                gr.FillEllipse(Brushes.LightGreen, rect);
                using (Pen thick_pen = new Pen(Color.Blue, 5))
                {
                    gr.DrawEllipse(thick_pen, rect);
                }
            }

            Panel.Image = bm;
            
        }

        private void LineWidth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(m_bmpCanvas, new Point(0, 0));

        }

        private void Panel_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}

